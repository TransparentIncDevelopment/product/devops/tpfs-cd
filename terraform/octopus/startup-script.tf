module "startup-script-lib" {
  source = "git::https://github.com/terraform-google-modules/terraform-google-startup-scripts.git?ref=v0.1.0"
}

data "template_file" "startup-script-config" {
  template = "${file("${path.module}/scripts/configure-octopus.tpl")}"
  vars = {
    tentacle_name = var.octopus-tentacle-name
    service_name = var.octopus-service-name
    api_key = data.vault_generic_secret.octopus.data["tentacle_api_key"]
    worker_pool_name = var.octopus-worker-pool-name
  }
}