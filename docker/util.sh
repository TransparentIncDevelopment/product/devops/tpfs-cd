#!/usr/bin/env bash

# Exit if error occurs
set -e

# Hacky way to check if fully qualified image exists. User must be authorized for given Container Registry
# This is used because gcr allows overwriting of tags - so we do this check so we can fail CI
# if a job tries to overwrite an existing tagged image.

# Example usage:
# `does_manifest_exist "gcr.io/xand-dev/some-image:some-tag"`
# will return codes of 0 when manifest already exists and 1 when it doesn't.
# See https://stackoverflow.com/a/43840545/1807040 for why we use return codes
# for a boolean function.
function does_manifest_exist {
    local OUTPUT;
    local RETCODE;

    # Capture stdout and stderr to confirm if error was "no such manifest"
    OUTPUT=$(DOCKER_CLI_EXPERIMENTAL=enabled docker manifest inspect $1 2>&1) && RETCODE=$? || RETCODE=$?

    if [[ $RETCODE -eq 0 ]]
    then
        return 0
    else
        if [[ $RETCODE -eq 1 ]] && [[ $OUTPUT =~ "no such manifest" ]]
        then
            return 1
        else
            >&2 echo "Error in checking manifest"
            >&2 echo $OUTPUT
            exit $RETCODE
        fi
    fi
}

# Example usage:
# `build_and_push_if_image_doesnt_exist "gcr.io/xand-dev/some-image:some-tag" cd-image $( dirname "${BASH_SOURCE[0]}" )`
function build_and_push_if_image_doesnt_exist {
    local IMAGE_NAME=$1
    local IMAGE_LOCAL_DIRECTORY=$2
    local DOCKER_DIR=$3

    does_manifest_exist "$IMAGE_NAME" && EXISTS=$? || EXISTS=$?

    if [[ $EXISTS -ne 0 ]];
    then
        echo "Working on" $IMAGE_NAME
        # Print commands
        set -x
        pushd $DOCKER_DIR/$IMAGE_LOCAL_DIRECTORY
        cp $VAULT_DEV_CA_PEM ./devCA.pem
        cp $VAULT_PROD_CA_PEM ./prodCA.pem
        docker build -t $IMAGE_NAME .
        docker push $IMAGE_NAME
        # Stop printing commands
        set +x
        popd
    else
        echo "Image manifest for $IMAGE_NAME exists. Continuing..."
    fi
}
