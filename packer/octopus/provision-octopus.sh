#!/usr/bin/env bash

set -o nounset
set -o errexit

export DEBIAN_FRONTEND=noninteractive
export APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn

sudo apt-get update -yqq
sudo apt-get install -yqq apt-transport-https software-properties-common gnupg2 dirmngr

echo "Fetch keys" 
sudo apt-key adv --fetch-keys https://apt.octopus.com/public.key
sudo add-apt-repository "deb https://apt.octopus.com/ stretch main"

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

echo "Second update"
sudo apt-get update -yqq

echo "Installing docker"
sudo apt-get install docker-ce docker-ce-cli containerd.io -yqq

echo "Installing tentacle"
sudo apt-get install tentacle -yqq

sudo /opt/octopus/tentacle/Tentacle create-instance --instance "$SERVICE_NAME" --config "/var/logs/octopus/$SERVICE_NAME/tentacle-$SERVICE_NAME.config"
sudo /opt/octopus/tentacle/Tentacle new-certificate --instance "$SERVICE_NAME" --if-blank
sudo /opt/octopus/tentacle/Tentacle configure --instance "$SERVICE_NAME" --app "/home/Octopus/Applications" --noListen "True" --reset-trust
