#!/usr/bin/env bash

set -o nounset
set -o errexit

sudo /opt/octopus/tentacle/Tentacle register-worker --instance "${service_name}" --server "https://tpfs.octopus.app" \
    --name "${tentacle_name}" --comms-style "TentacleActive" --server-comms-port "10943" --apiKey "${api_key}"  \
    --space "Default" --workerpool "${worker_pool_name}" --force
sudo /opt/octopus/tentacle/Tentacle service --install --start --instance "${service_name}"

sudo gcloud auth configure-docker
