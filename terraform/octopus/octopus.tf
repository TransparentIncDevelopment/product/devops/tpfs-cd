resource "google_compute_instance" "octopus" {
  name = "octopus-worker-${terraform.workspace}"
  machine_type = "n1-standard-1"
  zone = var.zone
  allow_stopping_for_update = true

  tags = [ "transparent-octopus" ]
  metadata_startup_script        = data.template_file.startup-script-config.rendered

  boot_disk {
    initialize_params {
      image = data.google_compute_image.octopus_image.self_link
    }
  }

  network_interface {
    network = var.network
    subnetwork = var.private_subnet
  }

  service_account {
    scopes = [ "compute-ro", "storage-ro" ]
  }


}
