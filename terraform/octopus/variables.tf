variable "project_id" {
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-b"
}

variable "network" {
    default = "default"
}

variable "private_subnet" {
    default = "private-clusters"
}

variable "octopus-service-name" {
}

variable "octopus-tentacle-name" {
}

variable "environment-short-name" {
}

variable "bucket-name" {
}

variable "octopus-worker-pool-name" {
}

variable "vault-addr" {
}