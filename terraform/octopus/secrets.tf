provider "vault" {
  address = var.vault-addr
}

data "vault_generic_secret" "octopus" {
  path = "secret/environments/${var.environment-short-name}/octopus"
}